/*
	What is a client?

	A client is an application which creates requests for resources from a server. A client will trigger an action, in the web development context, through a URL and wait for the response of the server.

*/
/*
	What is a server?

	A server is able to host and deliver resources requested by a client. In fact, a single server can handle multiple clients.
*/
/* 
	What is NodeJS?
	
	NodeJS is a runtime environment which allows us to create/develop backend/server-side applications with JavaScript.
	
	Because by default, JS was conceptualized solely to the front-end.

	Why is Node JS Popular?

	Performance ->? NodeJS is one of the most performing environment for creating backend applications with JS

	Familiarity -> Since NodeJS is built-around and uses JS as its language, it is very familiar for most developers.

	NPM -> Node Package Manager is the largest registry for node packages. Packages are bits of programs, methods, functions, and codes that greatly help in the development of an application.
*/

// require() is a built-in JS method which allows us to import packages
// packages are pieces of code we can integrate into our application

// "http" is a default package that comes with NodeJS.
// http is now a module in our application that works much like an object

// "http" module lets us create a server which is able to communicate with a client through the use of the Hypertext Transfer Protocol(https://).
const http = require("http");

// createServer() is a method from the http module. It allows us to create a server that us able to handle the request of a client and send a response to the client

// createServer has a function as an argument. This function handles the request and response.
// request is an object which contains the details of the response from the server.
// createServer() method ALWAYS receives the request object first before the response
http.createServer(function(request, response) {

	// writeHead() is a method of the respnse object which allows us to add headers to our response.
	// Headers are additional information aboput our response
	// It ALWAYS have 2 arguments, first is the HTTP Status Code, secondData Type of the response
	// HTTP Status Code is a numerical representation of the status of response
		// 200 - OK
		// 404 - Not Found
	response.writeHead(200, {'Content-Type': 'text/plain'});

	// end() is a method that ends our response. No more tasks will run after this code.
	// end() can send a message/data as a string.
	response.end('Hello NodeJS');

}).listen(4001); 

// listen() allows us to assign a port to our server. This will allows us to serve /run our index.js server in our local machine.
// Commonly unused ports are 4000, 4040, 8000, 5000, 3000, 4200.
